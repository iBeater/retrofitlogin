package com.example.amartineza.retrofitlogin.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by panqu on 24/03/2018.
 */

public class DataRequest {

    @SerializedName("card_number")
    private String cardNumber;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("transaction_include")
    private Boolean transactionInclue;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getTransactionInclue() {
        return transactionInclue;
    }

    public void setTransactionInclue(Boolean transactionInclue) {
        this.transactionInclue = transactionInclue;
    }

}
