package com.example.amartineza.retrofitlogin;

import com.example.amartineza.retrofitlogin.Models.DataRequest;
import com.example.amartineza.retrofitlogin.Models.DataResponse;
import com.example.amartineza.retrofitlogin.Models.LoginRequest;
import com.example.amartineza.retrofitlogin.Models.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by panqu on 25/03/2018.
 */

public interface Services {

    LoginResponse loginResponse = new LoginResponse();

    @FormUrlEncoded
    @Headers({"api_key:cinepolis_test_android",
            "Content-Type:application/x-www-form-urlencoded"})
    @POST("v2/oauth/token")
    Call<LoginResponse> getLogin(@Field("country_code") String contryCode,
                                 @Field("username") String userName,
                                 @Field("password") String pass,
                                 @Field("grant_type") String grantType,
                                 @Field("client_id") String clientId,
                                 @Field("client_secret") String clientSecret);

    @POST("v1/members/loyalty")
    Call<DataResponse> getMembersLoyalty(@Header("api_key") String apiKey ,
                                         @Header("Authorization") String token,
                                         @Body DataRequest bodyMembersLoyalti);

}
