package com.example.amartineza.retrofitlogin.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.amartineza.retrofitlogin.Models.DataRequest;
import com.example.amartineza.retrofitlogin.Models.DataResponse;
import com.example.amartineza.retrofitlogin.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by panqu on 24/03/2018.
 */

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ItemViewHolder>{
    private Context mContexto;
    private List<DataResponse.transaction> mList = new ArrayList<>();

    public DataAdapter(Context mContexto, List<DataResponse.transaction> mList) {
        this.mContexto = mContexto;
        this.mList = mList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final DataResponse.transaction model = mList.get(position);
        holder.tvCinema.setText(model.getCinema());
        holder.tvPoints.setText(model.getPoints()+"");

        String date1 = model.getDate();
        SimpleDateFormat parseador = new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss");
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd H:mm a");

        try {
            Date date = parseador.parse(date1);
            holder.tvDate.setText(formateador.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvCinema;
        TextView tvPoints;
        TextView tvDate;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tvCinema = (TextView)itemView.findViewById(R.id.tvCinema);
            tvPoints = (TextView)itemView.findViewById(R.id.tvPoints);
            tvDate = (TextView)itemView.findViewById(R.id.tvDate);
        }
    }
}
